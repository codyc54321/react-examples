#!/bin/bash

# 1. clone it
git clone https://bitbucket.org/codyc54321/react-examples.git
# cd into it
cd react-examples

# 2. update
if [ "$(uname)" == "Darwin" ]; then
    brew update
elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    sudo apt-get update

# 3. install python-pip
# https://stackoverflow.com/questions/17271319/how-do-i-install-pip-on-macos-or-os-x
curl -O https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py

# 4. make a virtual environment to isolate the python project
sudo pip install virtualenv
virtualenv venv

# 5. turn on virtualenv
source venv/bin/activate

# 6. install packages, like `npm install`
pip install -r requirements.txt

# 7. run server
python server.py
