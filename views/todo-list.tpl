% rebase('base.tpl')

var TodoItems = React.createClass({

    createTasks: function(item){
        return this.props.entries.map(item => {
            return (
                <li key={item.key}>
                    <span>{item.text + " "} </span>
                    <a  href="#" data-id="{item.id}"
                        className="remove-filter"
                        onClick={() => this.props.remove(item)}
                    >
                        remove
                    </a>
                </li>
            )
        });
    },

    render: function(){
        return (
            <ul className="theList">
                {this.createTasks()}
            </ul>
        );
    }
});

var TodoList = React.createClass({
    getInitialState: function(){
        return {
            items: [
                'If user clicks add button and input is empty, set a red border and focus the input box',
                'Allow interactive DOM update of component when user edits code box',
                'Allow categories for things like angular2, knockout, etc',
                'Allow user to save new code snippets after starting in a sandbox',
            ]
        };
    },

    addItem: function(e) {
        var itemArray = this.state.items;
        if (this._inputElement.value.length > 0){
            itemArray.push(
                {
                    text: this._inputElement.value,
                    key: this.state.items.length
                }
            );

            this._inputElement.value = "";
            this.setState({
                items: itemArray
            })
        }
        e.preventDefault();
    },

    // removing items from a list
    // http://stackoverflow.com/questions/27817241/how-to-remove-an-item-from-a-list-with-a-click-event-in-reactjs
    removeItem: function(item){
        var items = this.state.items.filter(function(itm){
            return item.key !== itm.key;
        });
        this.setState({ items: items });
    },

    render: function() {
        return (
            <div className="todoListMain">
                <div className="header">
                    <form onSubmit={this.addItem}>
                        <input ref={(a) => this._inputElement = a}
                            placeholder="enter task" />
                        <button type="submit">add</button>
                    </form>
                </div>
                <TodoItems remove={this.removeItem} entries={this.state.items} />
            </div>
        );
    }
});

ReactDOM.render(
  <div>
    <TodoList/>
  </div>,
  destination
);
