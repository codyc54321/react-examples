<!DOCTYPE html>
<html>
  
    <head>
        <title>{{name}}</title>
        <script src="https://unpkg.com/react@15.3.2/dist/react.js"></script>
        <script src="https://unpkg.com/react-dom@15.3.2/dist/react-dom.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.23/browser.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react-router/4.0.0-beta.3/react-router.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fixed-data-table/0.6.3/fixed-data-table.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/fixed-data-table/0.6.3/fixed-data-table.min.css" rel="stylesheet">

        <style>
            body {
              padding: 50px;
              background-color: #66CCFF;
              font-family: sans-serif;
            }
            .nav-link {
                margin-left: 12px;
            }

            a {
                color: #1e1d26;
            }

            #entry {
                width: 50%;
                margin: 0 auto;
            }

            #main {
                margin: 20px;
                font-size: 26px;
            }

            /*counter*/
            .counter-button {
                margin-left: 10px;
                padding: 12px;
                font-size: 18px;
            }

            /*todo list */
            .todoListMain .header input {
            padding: 10px;
            font-size: 16px;
            border: 2px solid #FFF;
            }

            .todoListMain .header button {
            padding: 10px;
            font-size: 16px;
            margin: 10px;
            background-color: #0066FF;
            color: #FFF;
            border: 2px solid #0066FF;
            }

            .todoListMain .header button:hover {
            background-color: #003399;
            border: 2px solid #003399;
            cursor: pointer;
            }

            .todoListMain .theList {
              list-style: none;
              padding-left: 0;
              width: 255px;
            }

            .todoListMain .theList li {
              color: #333;
              background-color: rgba(255,255,255,.5);
              padding: 15px;
              margin-bottom: 15px;
              border-radius: 5px;
            }
            /*end todo list*/


            /*validated email form*/
            .ContactView-title {
              font-size: 24px;
              padding: 0 24px;
            }

            .ContactView-list {
              list-style: none;
              margin: 0;
              padding: 0;
              border-top: 1px solid #f0f0f0;
            }

            .ContactItem {
              margin: 0;
              padding: 8px 24px;
              border-bottom: 1px solid #f0f0f0;
            }
            .ContactItem-email {
              font-size: 16px;
              font-weight: bold;
              margin: 0;
            }
            .ContactItem-name {
              font-size: 14px;
              margin-top: 4px;
              font-style: italic;
              color: #888;
            }
            .ContactItem-description {
              font-size: 14px;
              margin-top: 4px;
            }

            .ContactForm {
              padding: 8px 24px;
            }
            .ContactForm > input {
              display: block;
              width: 240px;
              padding: 4px 8px;
              margin-bottom: 8px;
              border-radius: 3px;
              border: 1px solid #888;
              font-size: 14px;
            }
            .ContactForm > input.ContactForm-error {
              border-color: #b30e2f;
            }
            /*end validated email form*/
        </style>

    <head>

      
    <body>
        <div style="margin-bottom: 35px">
            % for item in names:
                <a class="nav-link" href="/{{item}}">{{item}}</a>
            % end
        </div>


        % if tutorial:
            <p>
                <a href="{{tutorial}}" target="_blank">The tutorial link</a>
            </p>
        % end

        <div id="entry"></div>

        <div id="editor" style="height: 1500px; width: 100%;">{{ code }}</div>

        <script>
            function determineMode(filepath) {
                var modeString = "ace/mode/" + filepath;
                return modeString;
            }
        </script>

        <script src="/static/ace-builds2/src-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>

        <script>
            var editor = ace.edit("editor");
            editor.getSession().setMode(determineMode('html'));
            editor.session.setNewLineMode("unix");
            editor.setTheme("ace/theme/monokai");
            editor.setFontSize(15);
        </script>

        <script type="text/babel">
            var destination = document.querySelector("#entry");
            {{!base}}
        </script>
    </body>
  
</html>
